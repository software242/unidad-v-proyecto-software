#Importamos la paqueteria necesaria
import RPi.GPIO as GPIO
import time

TRIG=13 #Variable que contiene el GPIO al cual conectamos la señal TRIG del sensor
ECHO=12 #Variable que contiene el GPIO al cual conectamos la señal ECHO del sensor 
RELEBOMBA= 16 #Variable que contiene el GPIO el cual conectamos la señal del relé de la electrovalvúla
Disatanciamin= 5
Distanciamax= 15 #25



GPIO.setmode(GPIO.BOARD)  #Establecemos el modo según el cual nos refirimos a los GPIO de nuestra RPi
GPIO.setup(TRIG, GPIO.OUT) #Configuramos el pin TRIG como una salida
GPIO.setup(ECHO, GPIO.IN)  #Configuramos el ECHO como una salida
GPIO.setup(RELEBOMBA, GPIO.OUT) #configuramos el pin del relé de la bomba como una salida
GPIO.setup(RELEVALVULA, GPIO.OUT) #Configuramos el pin de relé de la valvúla como una salida



#Contenemos el código principal en un aestructura try para limpiar los GPIO al terminar o presentarse un error
try:
    #Implementamos un loop infinito
    while True:
        
        # Ponemos en bajo el pin TRIG y después esperamos 0.5 seg para que el transductor se estabilice
        GPIO.output(TRIG, GPIO. LOW)
        time.sleep(0.5)
        
        # Ponemos en alto el pin TRIG esperamos 10 uS antes de ponerlo en bajo
        GPIO.output(TRIG, GPIO.HIGH)
        time.sleep(0.00001)
        GPIO.output(TRIG, GPIO.LOW)
        
        # En este momento el sensor envía 8 pulsos ultrasónicos de 40kHz y coloca su pin ECHO en alto
        # Debemos detectar dicho evento para iniciar la medición del tiempo
        
        while True:
            pulso_inicio = time.time()
            if GPIO.input(ECHO) ==GPIO.HIGH:
                break
            
        # En el pin ECHO se mantendrá en HIGH hasta recibir el eco rebotando por el obstaculo.
        # En ese momento el sensor pondrá el pin ECHO eb bajo
        # Procedemos a detectar dicho evento para terminar la medición del tiempo
        
        while True:
            pulso_fin = time.tiem()
            if GPIO.input(ECHO) == GPIO.LOW:
                break
            
        # Tiempo medido en segundos
        duracion= pulso_fin - pulso_inicio
        
        # Obtenemos la distancia considerando que la señal recorre dos veces la distancia a medir y que la velocidad del
        distancia = (34300* duracion) / 2
        
        # Imprimimos los resultado
        print( "Distancia: %2f cm" % distancia)
        
        GPIO.output(RELEBOMBA, GPIO.LOW)
        GPIO.OUTPUT(RELEVALVULA, GPIO.HIGH)
        if distancia < Distanciamin:
            GPIO.output(RELEBOMBA, GPIO.LOW)
            GPIO:output(RELEVALVULA, GPIO.HIGH)
            time.sleep(7)
            
        if distancia > Distanciamax:
            GPIO.output(RELEBOMBA, GPIO.HIGH)
            GPIO.output(RELEVALVULA, GPIO.LOW)
            time.sleep(7)
            
finally:
        GPIO.cleanup()
        